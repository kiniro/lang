(def Y (f)
  ((\ x -> (x x))
   (\ y ->
     (f (\->
          (apply (y y) args))))))

(def fac (f)
  (\ n ->
    (if (== 0 n)
	1
	(* n (f (- n 1))))))

(def fib (f)
  (\n ->
    (cond
      ((== n 0) 0)
      ((== n 1) 1)
      (true (+ (f (- n 1))
		    (f (- n 2)))))))

;; See [1] for explanation.

;; Count how many times f called itself recursively when
;; called with given arg
(def count-calls (f arg)
  (begin
    (def counter 0)
    (def wrap (f)
      (\g -> (\p ->
        (let ((result ((f g) p)))
          (do (set counter (+ 1 counter))
              result)))))
    [
    ;; result
    ((Y (wrap f)) arg)
    ;; number of calls to f
    counter
    ]))

;; Function that creates a new recursive call counter
;; and returns a wrapper and a getter for the counter.
(def make-counter ()
  (do
    ;; Counter variable - will be trapped in a closure
    (def counter 0)
    ;; Getter to return
    (def get-counter () counter)
    ;; Wrapper as described in [1]
    (def wrap (f)
      (\g -> (\p ->
        (let ((result ((f g) p)))
          (do (set counter (+ 1 counter))
              result)))))
    ;; Return both functions that share the same state
    [wrap get-counter]))

[
 ((Y fac) 3) ;; 6
 ((Y fib) 6) ;; 8

 (count-calls fib 4) ;; [3, 9]
 (count-calls fac 5) ;; [120, 6]

 ;; deconstruct a list returned by make-counter
 (let ((wc (make-counter))
       (wrap (head wc))
       (get-counter (head (tail wc))))
   [ ;; Call a wrapped function using Y-combinator
    ((Y (wrap fib)) 5)
    ;; Extract closure state
    (get-counter)
    ;; Call another time
    ((Y (wrap fib)) 5)
    ;; Counter value is two times greater
    (get-counter)]) ;; [5, 15, 5, 30]
  ;; make sure that the new counter is 0
  ((head (tail (make-counter)))) ;; 0
]

;; [1] http://www.lfcs.inf.ed.ac.uk/reports/97/ECS-LFCS-97-375/ - 3.1 Simple wrappers
