const assert = require('assert');

const {
    Kiniro,
    KiniroCore,
    symbol,
    getType,
    unwrap,
    wrap,
    nil,
    parenth,
    cons,
    object,
    listToArray,
    arrayToList,
    seq,
    lift, unlift,
    types,
    keywords
} = require('../index.js');

describe('JS <-> Kiniro interoperability', () => {
    describe('from js', () => {
        it('using functions defined inside kiniro', async () => {
            const kiniro = new Kiniro();
            const res = await kiniro.exec(
                '(def reduce (f e xs)\
                    (if (empty xs) \
                        e \
                      (f (head xs) (reduce f e (tail xs)))))'
            );

            assert.deepStrictEqual(await kiniro.lookup('reduce')(
                (x, y) => x + y,
                0,
                [1,2,3,4]
            ), 10);
        });
    });
});
