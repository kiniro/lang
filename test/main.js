const assert = require('assert');
const fs = require('fs');

const {
    Kiniro,
    KiniroCore,
    symbol,
    getType,
    unwrap,
    wrap,
    nil,
    parenth,
    cons,
    object,
    listToArray,
    arrayToList,
    seq,
    lift, unlift,
    types,
    keywords
} = require('../index.js');

const escapeSeqs = {
    '"': '"',
    '\\': '\\',
    'b': '\b',
    'f': '\f',
    'n': '\n',
    'r': '\r',
    't': '\t'
};

const shouldFail = (promise, errValidation) => new Promise ((resolve, reject) => {
    errValidation = errValidation || (x => true);

    promise.then(reject).catch(x => {
        if (errValidation(x)) {
            resolve(false);
        } else {
            reject("error is invalid: " + x);
        }
    });
});

const loadCode = (name) => {
    return fs.readFileSync('./test/lang/'+name).toString();
};

/** Convert CONS/NIL list to Array with unwrapped elements */
function listToArrayValues (lst) {
    return listToArray(lst).map(unwrap);
}

describe('arrayToList', () => {
    it('should pass tests', async () => {
        const tests = [
            ['[1 2 3]', [1, 2, 3]]
        ];

        for (let [code, res] of tests)  {
            let result = await new Kiniro().run(code);
            assert.deepStrictEqual(result, lift(res));
        }
    });
});

describe('listToArrayValues', () => {
    it('should pass tests', async () => {
        const tests = [
            ['[1 2 3]', [1, 2, 3]]
        ];

        for (let [code, res] of tests)  {
            let result = await new Kiniro().run(code);
            assert.deepStrictEqual(result, lift(res));
        }
    });
});

var tests = [

    // Math
    ...Object.getOwnPropertyNames(Math).map(name => {
        if (typeof Math[name] == 'function' && name != 'random') {
            let v1 = 0.123, v2 = 0.456;
            return [
                `Math.${name}`,
                `(Math.${name} ${v1} ${v2})`,
                Math[name](v1, v2)
            ];
        } else {
            return [
                `Math.${name}`,
                `Math.${name}`,
                Math[name]
            ];
        }
    }),

    /* examples from files */
    [
        "test/lang/append.kiniro",
        loadCode('append.kiniro'),
        lift([1, 2, 3, 4, 5])
    ],

    [
        "test/lang/range.kiniro",
        loadCode('range.kiniro'),
        lift([0, 2, 4, 6, 8])
    ],

    [
        "test/lang/quicksort.kiniro",
        loadCode('quicksort.kiniro'),
        lift([1, 1, 1, 1, 2, 4, 6, 6, 9, 9])
    ],

    [
        "test/lang/fixpoint.kiniro",
        loadCode('fixpoint.kiniro'),
        lift([6,
              8,
              [3,  9],
              [120, 6],
              [5, 15, 5, 30],
              0])
    ],

    [
        "test/lang/maximumBy.kiniro",
        loadCode('maximumBy.kiniro'),
        lift({v:2})
    ],

];

describe("kiniro-lang", () => {
    var demos = {};
    tests.forEach(test => {
        it(test[0] + ":\n         " + test[1]
         + (['number', 'boolean'].includes(typeof test[2]) ? " => " + test[2] : "") ,
           function () {
               if (!test[3]) { test[3] = {}; };
               var options = test[3];
               const kinOptions = {};
               if (options.prelude) {
                   kinOptions.prelude = options.prelude;
               }

               var kiniro = new Kiniro(kinOptions), promise;

               if (!options.disableDemo) {
                   demos[test[0]] = test[1];
               }

               if (options.disabled) {
                   return;
               }

               if (!options.log) {
                   kiniro.logger = function () {};
               } else {
                   kiniro.logger = console.log;
               }

               if (!!options.echo) {
                   kiniro.echo = options.echo;
               }

               if (options.timeout) {
                   this.timeout(options.timeout);
               }

               promise = kiniro.run(test[1]);

               if (options.minTime) {
                   promise = new Promise((resolve, reject) => {
                       let flag = false;
                       setTimeout(() => flag = true, options.minTime);
                       promise.then(result => {
                           assert(flag, 'returned earlier than expected');
                           return result;
                       }).then(resolve).catch(reject);
                   });
               }

               if (options.error) {
                   return shouldFail(promise,
                                     typeof test[2] == 'function' ?
                                     test[2] : undefined);
               }

               if (typeof test[2] === 'function') {
                   return promise.then(test[2]);
               } else {
                   return promise.then(r => {
                       if (options.error) {
                           return;
                       }

                       if (options.show) {
                           console.log('EXPR----------------------\n',
                                       showExpression(r),
                                       '\n----------------------');
                       }

                       if (test[2] instanceof Array) {
                           assert.deepStrictEqual(r, test[2]);
                       } else {
                           assert.deepStrictEqual(unwrap(r), test[2]);
                       }
                   });
               }
           });
    });
});
