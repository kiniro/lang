const assert = require('assert');

const {
    Kiniro,
    KiniroCore,
    symbol,
    getType,
    unwrap,
    wrap,
    nil,
    parenth,
    cons,
    object,
    listToArray,
    arrayToList,
    seq,
    lift, unlift,
    types,
    keywords
} = exports = require('../index.js');
const core = require('@kiniro/core');
const prelude = require('@kiniro/prelude');

describe('package exports', () => {
    it('should contain everything that was exported from @kiniro/core', () => {
        Object.keys(core).forEach(key => {
            if (typeof exports[key] == 'undefined') {
                throw ("module does not export " + key);
            }
        });
    });
    it('...and Kiniro', () => {
        assert(typeof exports.Kiniro == 'function');
    });
});

describe('Kiniro', () => {
    it('shouldn\'t throw on creation', () => {
        new Kiniro();
    });

    describe('should contain stuff from prelude', () => {
        const kiniro = new Kiniro();
        Object.keys(prelude.raw).forEach(key => {
            it(key, () => {
                kiniro.env.lookup(kiniro.symbols.get(key));
            });
        });

        Object.keys(prelude.lifted).forEach(key => {
            it(key, () => {
                kiniro.env.lookup(kiniro.symbols.get(key));
            });
        });
    });
});
