const assert = require('assert');

const {
    Kiniro,
    KiniroCore,
    symbol,
    getType,
    unwrap,
    wrap,
    nil,
    parenth,
    cons,
    object,
    listToArray,
    arrayToList,
    seq,
    lift, unlift,
    types,
    keywords
} = require('../index.js');

class MyObject extends Object {
    constructor (props) {
        super();
        Object.entries(props).forEach(([key, value]) => {
            this[key] = value;
        });
    }
}

describe('objects', () => {
    it('prototypes of MyObject and Object instances differ', () => {
        let o = {};
        assert(Object.getPrototypeOf(new MyObject(o)) !== Object.getPrototypeOf(new Object(o)));
    });

    describe('wrapped objects', () => {
        it('getting properties - 1', async () => {
            const kiniro = new Kiniro();
            kiniro.define('x', new MyObject({y:1}));
            const res = await kiniro.run('x.y');
            assert.deepStrictEqual(res, lift(1));
        });

        it('getting properties - 2', async () => {
            const kiniro = new Kiniro();
            kiniro.define('x', new MyObject({y: new MyObject({z:2})}));
            const res = await kiniro.run('x.y.z');
            assert.deepStrictEqual(res, lift(2));
        });

        it('getting properties - 3', async () => {
            const kiniro = new Kiniro();
            kiniro.define('x', new MyObject({y: new MyObject({z:2})}));
            const res = await kiniro.run('x.y');
            assert.deepStrictEqual(res, lift(new MyObject({ z: 2 })));
        });

        it('setting properties - 1', async () => {
            const kiniro = new Kiniro();
            kiniro.define('x', new MyObject({y: 1}));
            const res = await kiniro.run('(set x.y 2) x');
            assert.deepStrictEqual(res, lift(new MyObject({ y: 2 })));
        });

        it('object mutation using set - 1', async () => {
            const kiniro = new Kiniro();
            const myObject = new MyObject({y: 1});
            kiniro.define('x', myObject);
            const res = await kiniro.run('(set x.y 2) x');
            assert.deepStrictEqual(myObject.y, 2);
        });

        it('object mutation using set - setting property value to function', async () => {
            const kiniro = new Kiniro();
            const myObject = new MyObject({y: 1});
            kiniro.define('x', myObject);
            const res = await kiniro.exec('(def f (x) (+ 1 x)) (set x.y 2) (set x.f f)');
            assert.deepStrictEqual(await kiniro.lookup('x').f(10), 11);
        });

        it('object mutation using set', async () => {
            const kiniro = new Kiniro();
            const myObject = new MyObject({y: 1});
            kiniro.define('x', myObject);
            const res = await kiniro.exec('(set x.y 2) x.y');
            assert.deepStrictEqual(res, 2);
        });
    });
});
