const core = require('@kiniro/core');
const parseRaw = require('@kiniro/parser');
const prelude = require('@kiniro/prelude');

/** Extends {@link KiniroCore} by providing parse function from
    {@link https://gitlab.com/kiniro/parser @kiniro/parser}
    and populating the default environment with
    {@link https://gitlab.com/kiniro/prelude @kiniro/prelude}. */
class Kiniro extends core.KiniroCore {
    constructor (opts) {
        super(opts);
        this.parseRaw = parseRaw;
        this.load(prelude);
    }
}

core.Kiniro = Kiniro;
module.exports = core;
